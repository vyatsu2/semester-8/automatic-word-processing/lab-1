from Lab_1.ending_classes import EndingClasses


class StemmerPorter:
    vowels = ['а', 'е', 'и', 'о', 'у', 'ы', 'э', 'ю', 'я']

    endings = {
        EndingClasses.PERFECTIVE_GERUND_1: ['в', 'вши', 'вшись'],
        EndingClasses.PERFECTIVE_GERUND_2: ['ив', 'ивши', 'ившись', 'ыв', 'ывши', 'ывшись'],
        EndingClasses.ADJECTIVE: ['ее', 'ие', 'ые', 'ое', 'ими', 'ыми', 'ей', 'ий', 'ый', 'ой', 'ем', 'им', 'ым', 'ом',
                                  'его', 'ого', 'ему', 'ому', 'их', 'ых', 'ую', 'юю', 'ая', 'яя', 'ою', 'ею'],
        EndingClasses.PARTICIPLE_1: ['ем', 'нн', 'вш', 'ющ', 'щ'],
        EndingClasses.PARTICIPLE_2: ['ивш', 'ывш', 'ующ'],
        EndingClasses.REFLEXIVE: ['ся', 'сь'],
        EndingClasses.VERB_1: ['ла', 'на', 'ете', 'йте', 'ли', 'й', 'л', 'ем', 'н', 'ло', 'но', 'ет', 'ют', 'ны', 'ть',
                               'ешь', 'нно'],
        EndingClasses.VERB_2: ['ила', 'ыла', 'ена', 'ейте', 'уйте', 'ите', 'или', 'ыли', 'ей', 'уй', 'ил', 'ыл', 'им',
                               'ым', 'ен', 'ило', 'ыло', 'ено', 'ят', 'ует', 'уют', 'ит', 'ыт', 'ены', 'ить', 'ыть',
                               'ишь', 'ую', 'ю'],
        EndingClasses.NOUN: ['а', 'ев', 'ов', 'ие', 'ье', 'е', 'иями', 'ями', 'ами', 'еи', 'ии', 'и', 'ией', 'ей', 'ой',
                             'ий', 'й', 'иям', 'ям', 'ием', 'ем', 'ам', 'ом', 'о', 'у', 'ах', 'иях', 'ях', 'ы', 'ь',
                             'ию', 'ью', 'ю', 'ия', 'ья', 'я'],
        EndingClasses.SUPERLATIVE: ['ейш', 'ейше'],
        EndingClasses.DERIVATIONAL: ['ост', 'ость'],
    }

    @staticmethod
    def create_adjectival(endings: dict[EndingClasses, list]):
        adjective = endings[EndingClasses.ADJECTIVE]
        participle = endings[EndingClasses.PARTICIPLE_1]
        participle.extend(endings[EndingClasses.PARTICIPLE_2])
        adjectival = []
        adjectival.extend(adjective)
        adjectival.extend(participle)
        combination = []
        for part in participle:
            for adj in adjective:
                combination.append(part + adj)
        adjectival.extend(combination)
        return adjectival

    @staticmethod
    def sorted_endings(endings: dict[EndingClasses, list]):
        result = {}
        for ending_class, ending_list in endings.items():
            result[ending_class] = sorted(ending_list, key=lambda item: len(item), reverse=True)
        return result

    def start(self, word: str):
        print(f'\nПроверяется слово {word}')
        if word is None or word == '': return word
        # self.endings[EndingClasses.ADJECTIVAL] = self.create_adjectival(self.endings)
        self.endings = self.sorted_endings(self.endings)

        # Шаг первый
        print('Шаг первый')
        ending = self.step_first(word)
        word = self.delete_ending(word, ending)

        # Шаг второй
        print('Шаг второй')
        rv = self.find_rv(word)
        if rv is None or rv == '': return word
        if rv.endswith('и'):
            word = word.removesuffix('и')

        # Шаг третий
        print('Шаг третий')
        rv = self.find_rv(word)
        if rv is None or rv == '': return word
        r1 = self.find_r(rv)
        r2 = self.find_r(r1)
        ending, is_find = self.find_ending_by_class(r2, EndingClasses.DERIVATIONAL)
        word = self.delete_ending(word, ending)

        # Шаг четвёртый
        print('Шаг четвёртый')
        rv = self.find_rv(word)
        if rv is None or rv == '': return word
        if rv.endswith('нн'):
            word = word.removesuffix('н')
            return word

        ending, is_find = self.find_ending_by_class(rv, EndingClasses.SUPERLATIVE)
        word = self.delete_ending(word, ending)
        if word.endswith('нн'):
            word = word.removesuffix('н')
            return word

        return word.removesuffix('ь')

    def step_first(self, word) -> str:
        rv = self.find_rv(word)
        if rv is None or rv == '': return ''
        ending, is_find = self.find_with_check(rv, EndingClasses.PERFECTIVE_GERUND_1, ['а', 'я'])
        if is_find:
            print('Найден PERFECTIVE_GERUND_1')
            return ending

        ending, is_find = self.find_ending_by_class(rv, EndingClasses.PERFECTIVE_GERUND_2)
        if is_find:
            print('Найден PERFECTIVE_GERUND_2')
            return ending

        ending_1, is_find = self.find_ending_by_class(rv, EndingClasses.REFLEXIVE)
        rv = self.delete_ending(rv, ending)

        ending_2, is_find = self.find_adjectival(rv)
        if is_find:
            print('Найден ADJECTIVAL')
            return ending_1 + ending_2

        ending_2, is_find = self.find_with_check(rv, EndingClasses.VERB_1, ['а', 'я'])
        if is_find:
            print('Найден VERB_1')
            return ending_1 + ending_2

        ending_2, is_find = self.find_ending_by_class(rv, EndingClasses.VERB_2)
        if is_find:
            print('Найден VERB_2')
            return ending_1 + ending_2

        ending_2, is_find = self.find_ending_by_class(rv, EndingClasses.NOUN)
        print('Найден NOUN')
        return ending_1 + ending_2

    def find_and_delete(self, word: str, ending_class: EndingClasses) -> (str, bool):
        ending = self.find_ending(word, self.endings[ending_class])
        if ending != '':
            return self.delete_ending(word, ending), True
        return word, False

    def find_and_delete_with_check(self, word: str, ending_class: EndingClasses, pref: list[str]) -> (str, bool):
        ending = self.find_ending(word, self.endings[ending_class])
        if ending != '' and word[len(word) - 1 - len(ending)] in pref:
            return self.delete_ending(word, ending), True
        return word, False

    def find_adjectival(self, word):
        ending_1, is_find = self.find_ending(word, self.endings[EndingClasses.ADJECTIVE])
        if is_find:
            word = self.delete_ending(word, ending_1)
            ending_2, is_find = self.find_with_check(word, EndingClasses.PARTICIPLE_1, ['а', 'я'])
            if is_find:
                return ending_2 + ending_1, True
            ending_2, is_find = self.find_ending(word, self.endings[EndingClasses.PARTICIPLE_2])
            if is_find:
                return ending_2 + ending_1, True
            return ending_1, True
        return '', False


    def find_with_check(self, word: str, ending_class: EndingClasses, pref: list[str]) -> (str, bool):
        ending, is_find = self.find_ending_with_check(word, self.endings[ending_class], pref)
        if is_find and ending != '' and word[len(word) - 1 - len(ending)] in pref:
            return ending, True
        return word, False

    @staticmethod
    def find_ending(word: str, endings: list) -> (str, True):
        if word is None or word == '': return '', False
        for index in range(len(endings)):
            if word.endswith(endings[index]):
                print(f'Для {word} найдено окончание {endings[index]}')
                return endings[index], True
        return '', False

    @staticmethod
    def find_ending_with_check(word: str, endings: list, pref: list[str]) -> (str, bool):
        if word is None or word == '': return '', False
        for index in range(len(endings)):
            if word.endswith(endings[index]) and word[len(word) - 1 - len(endings[index])] in pref:
                print(f'Для {word} найдено окончание {endings[index]}')
                return endings[index], True
        return '', False

    def find_ending_by_class(self, word: str, ending_class: EndingClasses) -> (str, bool):
        if word is None or word == '': return '', False
        for index in range(len(self.endings[ending_class])):
            if word.endswith(self.endings[ending_class][index]):
                print(f'Для {word} найдено окончание {self.endings[ending_class][index]}')
                return self.endings[ending_class][index], True
        return '', False

    @staticmethod
    def delete_ending(word: str, ending: str) -> str | None:
        if word is None: return None
        return word.removesuffix(ending)

    def find_rv(self, word: str) -> str:
        for index in range(len(word)):
            if word[index] in self.vowels:
                return word[index + 1:]

    def find_r(self, word) -> str:
        if word is None or len(word) < 2: return None
        for index in range(len(word) - 1):
            if word[index] in self.vowels and word[index + 1] not in self.vowels:
                return word[index+2:]
