from enum import Enum


class EndingClasses (Enum):
    PERFECTIVE_GERUND_1 = 0
    PERFECTIVE_GERUND_2 = 1
    ADJECTIVE = 2
    PARTICIPLE_1 = 3
    PARTICIPLE_2 = 4
    REFLEXIVE = 5
    VERB_1 = 6
    VERB_2 = 7
    NOUN = 8
    SUPERLATIVE = 9
    DERIVATIONAL = 10
    ADJECTIVAL = 11

