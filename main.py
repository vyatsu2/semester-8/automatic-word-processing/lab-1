from Lab_1.stemmer_porter import StemmerPorter

def read_text(path: str):
    file = open(path, 'r')
    text = file.read()
    words = list(map(trim_word, text.split()))
    file.close()
    print(words)
    return words

def trim_word(word: str):
    return word.strip(' ,.!?-—;:"').lower()

if __name__ == "__main__":
    words = read_text(input('Введите путь к файлу: '))
    for word in words:
        print(f'Основа слова: {StemmerPorter().start(word)}')